package com.devsisters.kafkastreamjava;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

@Slf4j
@SpringBootApplication
public class WordCountStreamApplication {

    public static void main(String[] args) {
        Properties config = new Properties();

        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "poc-condition-count"); // 카프카 클러스터에 등록될 애플리케이션의 고유이름
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092, 127.0.0.1:9093"); // 카프카 클러스터
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass()); // 메시지 키 Ser / Des
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass()); // 메시지 값 Ser / Des

        final StreamsBuilder builder = new StreamsBuilder();

        final KStream<String, String> sentences = builder.stream("test_topic_01");
        final KStream<String, String> flatWord = sentences
                .flatMapValues(v -> Arrays.asList(v.toLowerCase().split("\\W+")))
                .map((k, v) -> {
                    log.info("map ==> {}:{}", k, v);
                    return new KeyValue<>(k, v);
                });
        //        .to("streams-pipe-outputs")
        ;

        final KTable<String, Long> wordCount = flatWord.groupBy(new KeyValueMapper<String, String, String>() {
            @Override
            public String apply(String key, String value) {
                return value;
            }
        }).count();

        wordCount.toStream().foreach((k, v) -> {
            log.info("word count {}:{}", k, v);
        });

        final KafkaStreams streams = new KafkaStreams(builder.build(), config);
        final CountDownLatch latch = new CountDownLatch(1);

        Runtime.getRuntime().addShutdownHook(new Thread("streams-condition-count") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }

    public String apply(String key, String value) {
        return value;
    }
}

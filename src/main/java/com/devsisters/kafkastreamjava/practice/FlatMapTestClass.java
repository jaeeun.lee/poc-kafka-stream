package com.devsisters.kafkastreamjava.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlatMapTestClass {
    public static void main(String[] args) {
        List<Integer> i1= Arrays.asList(1, 2, 3, 4);
        List<Integer> i2= Arrays.asList(5, 6, 7, 8);

        List<List<Integer>> doublelist = Arrays.asList(i1, i2);
        System.out.println("List<List<Integer>>" + doublelist);
        List<Integer> flatlist = doublelist.stream()
                .flatMap(x -> x.stream())
                .collect(Collectors.toList());
        System.out.println("List<Integer>" + flatlist);
    }
}

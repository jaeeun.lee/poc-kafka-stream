package com.devsisters.kafkastreamjava.practice;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class MapTestClass {
    public static void main(String[] args) {
        System.out.println(">>>>>>>>>>>>MapTestClass");

        List<Person> sample = Arrays.asList(
                new Person(20, "Lee"),
                new Person(35, "kyung"),
                new Person(67, "seok"),
                new Person(10, "test man"),
                new Person(45, "test woman")
        );

        // List -> Stream<Person> -> map -> Stream<String>
        // Stream<String> mapStream = sample.stream().map(p -> p.getName());
        // mapStream.forEach(System.out::println);

        // Without map()
        Stream<Person> stream = sample.stream().filter(p -> p.getName().contains("test"));
        stream.forEach(person -> {System.out.println("Without map:" + person.getName());});

        // With map()
        Stream<String> mapStream = sample.stream()
                .filter(p -> p.getName().contains("test"))
                .map(Person::getName)
                ;
        mapStream.forEach(System.out::println);
    }

    @Getter
    static class Person {
        int age;
        String name;

        public Person(int age, String name) {
            this.age = age;
            this.name = name;
        }
    }
}


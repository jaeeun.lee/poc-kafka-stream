package com.devsisters.kafkastreamjava.producer;

import com.sun.javafx.binding.Logging;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;

import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Producer implements Runnable {
    static final Logger log = Logger.getLogger(Producer.class.getName());
    static final String APPLICATION_ID = "streams-pipe";
    static final String KAFKA_CLUSTER = "127.0.0.1:9092";
    static final String KEY_PREFIX = "machine_";
    static final String TOPIC = "metrics-topic";

    @Override
    public void run() {
        try {
            producer();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public static void producer() {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_CLUSTER);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.ACKS_CONFIG, "0");

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(props);
        ProducerRecord<String, String> record = null;

        try {
            Random rnd = new Random();

            for (int i = 0; i <= 5; i++) {
                String key = KEY_PREFIX + i;
                String value = String.valueOf(rnd.nextInt(20));
                record = new ProducerRecord<>(TOPIC, key, value);

                RecordMetadata meta = producer.send(record).get();
                log.log(Level.INFO, "Partitioning for key-value {0}::{1} is {2}", new Object[]{key, value, meta.partition()});
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "Producer thread was interrupted");
        } finally {
            producer.close();
            log.log(Level.INFO, "Producer Close");
        }
    }
}

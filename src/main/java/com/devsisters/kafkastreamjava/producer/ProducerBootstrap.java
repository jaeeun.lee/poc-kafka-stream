package com.devsisters.kafkastreamjava.producer;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class ProducerBootstrap {
    private static final Logger log = Logger.getLogger(ProducerBootstrap.class.getName());

    public static void main(String[] args) {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

        executor.scheduleWithFixedDelay(new Producer(), 5, 10, TimeUnit.SECONDS);
        log.info("Kafka producer triggered");
    }
}

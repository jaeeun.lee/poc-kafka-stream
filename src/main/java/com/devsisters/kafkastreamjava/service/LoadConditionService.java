package com.devsisters.kafkastreamjava.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Slf4j
@Service
public class LoadConditionService {

    @PostConstruct
    public void initialize() {
        log.info(">>>>>>>>>>>>>>>>>>>>>>>> LoadConditionService");
    }

}
